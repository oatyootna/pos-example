-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 05:40 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_example`
--

-- --------------------------------------------------------

--
-- Table structure for table `buyhistory`
--

CREATE TABLE `buyhistory` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_thai_520_w2 NOT NULL,
  `productName` varchar(255) COLLATE utf8mb4_thai_520_w2 NOT NULL,
  `price` float(10,2) NOT NULL,
  `amount` int(11) NOT NULL,
  `create_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_thai_520_w2;

--
-- Dumping data for table `buyhistory`
--

INSERT INTO `buyhistory` (`id`, `username`, `productName`, `price`, `amount`, `create_at`) VALUES
(4, 'oat', 'Red dead redemption 2', 65.00, 2, '2019-10-07 14:33:36.000000'),
(5, 'oat', 'Pikachu', 50.00, 1, '2019-10-07 14:33:37.000000'),
(6, 'oat', 'Pikachu', 50.00, 2, '2019-10-07 14:44:33.000000'),
(7, 'oat', 'Minecraft', 50.00, 1, '2019-10-07 14:44:33.000000');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE current_timestamp(6),
  `path_to_images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `amount`, `created_at`, `updated_at`, `path_to_images`) VALUES
(2, 'Red dead redemption 2', '', 65.00, 97, '2019-09-21 16:46:29.000000', '2019-10-07 15:09:09.258734', 'images/Red-Dead-Redemption-2.jpg'),
(14, 'Pikachu', '', 50.00, 97, '2019-10-07 14:24:14.000000', '2019-10-07 14:44:33.039098', 'images/poe.jpg'),
(15, 'Minecraft', '', 50.00, 99, '2019-10-07 14:31:11.000000', '2019-10-07 14:44:33.042044', 'images/512dVKB22QL.png');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `productName` varchar(255) COLLATE utf8mb4_thai_520_w2 NOT NULL,
  `discountPercent` int(10) NOT NULL,
  `original_price` float(10,2) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `expired_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_thai_520_w2;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE current_timestamp(6),
  `login_status` int(1) NOT NULL,
  `last_activity_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `isAdmin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`, `login_status`, `last_activity_time`, `isAdmin`) VALUES
(13, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', '2019-09-29 14:26:49.000000', '2019-10-07 15:34:03.388397', 1, '2019-10-07 15:34:03.000000', 1),
(14, 'oat', '7e65ff153ca4b85c65a7b53e2b876a7f39f901660a157180e1bdfbdbe039910c1d1911b7258f2cc3ef6b2ecfbbd3d44778482b101d4d6ffe39bad5dd6298e6eb', '2019-09-29 14:57:34.000000', '2019-10-07 15:32:10.367812', 1, '2019-10-07 15:32:10.000000', 0),
(16, 'prayoot', '8e0e0b44d5e43490b48ee4ede9bff71c1f8c0dae27742078fcd377a916a1779729ee42633d74e13dbd660cf282f8ce431176465623da69f9ad5245e38b3340a4', '2019-10-05 15:42:33.000000', '2019-10-05 15:42:45.241319', 1, '2019-10-05 15:42:45.000000', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buyhistory`
--
ALTER TABLE `buyhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`productName`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buyhistory`
--
ALTER TABLE `buyhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
