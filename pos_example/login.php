<?php
session_start();
define('TIMEZONE', 'Asia/Bangkok');
$conn = new mysqli('localhost', 'root', '', 'pos_example');
$_SESSION['username'] = null;
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$usernameErr = "";
$passwordErr = "";
$err = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_POST['username'] === "") {
        $usernameErr = "Please enter username";
    } else {

        $hashedPassword = hash('sha512', $_POST['password']);
        $sql = "SELECT * FROM users WHERE username='" . $_POST['username'] . "' and password='" . $hashedPassword . "' ";
        $result = mysqli_query($conn, $sql);
        // echo $result ;
        if (mysqli_num_rows($result)) {

            $obj = mysqli_fetch_array($result);
            $sql = "UPDATE users SET login_status = 1 , last_activity_time = NOW() WHERE username = '" . $_POST['username'] . "' ";
            $result = mysqli_query($conn, $sql);
            $_SESSION['username'] = $obj['username'];
            $_SESSION['isAdmin'] = $obj['isAdmin'];
            if ($obj['isAdmin']) {
                header("Location: products.php");
            } else {
                header("Location: shopping.php");
            }
        } else {
            $err = "Wrong Username or Password";
        }
    }
}
$conn->close();
?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="./vendors/css/loginPage.css">
    <!-- <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css"> -->
    <title>Login Game Store Page</title>
</head>

<body>
    <div class="web-bg"></div>
    <div class="container">
        <div class="login-box">
            <div class="row">
                <p style="font-size: 65px; color: #12335a"> OS2</p>

                <h6>This is Game Store website for Operating System2 project</h6>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" id="loginform">
                    <div class="container-fluid" style="width: 300px; height:200px;">
                        <div class="row" style="padding: 2px;">
                            <input style="width: 290px; height:50px" type="text" name="username" placeholder="Enter Username">
                            <p style="color: red"> <?php echo $usernameErr ?></p>
                        </div>
                        <div class="row" style="padding: 2px;">
                            <input style="width: 290px; height:50px" type="password" name="password" placeholder="Enter Password">
                            <p style="color: red"> <?php echo $passwordErr ?></p>
                        </div>
                        <div class="row" style="padding: 2px;">
                            <p style="color: red"> <?php echo $err ?></p>
                            <button class="login-button" type="summit" form="loginform"> SIGN IN</button>

                        </div>
                    </div>
                </form>
                <hr style="border: 3px solid #3a3a3bbb">
                <a class="register-link" href="register.php" style="left: 20%;position: absolute;">
                    SIGN UP
                </a>
            </div>
        </div>
    </div>
</body>

</html>