<?php

session_start();
if (!isset($_SESSION['username'])  || $_SESSION['isAdmin'] == 0) {
  header("Location: login.php");
} else {
  define('TIMEZONE', 'Asia/Bangkok');
  $conn = new mysqli('localhost', 'root', '', 'pos_example');
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  if (!empty($data) && $data->type === 'CREATE') {
    $query = $conn->prepare('INSERT INTO users (username, password, created_at, updated_at, login_status,last_activity_time, isAdmin) VALUES (?, ?, NOW(), NOW(), 0, "0000-00-00 00:00:00", ?)');
    $hashedPassword = hash('sha512', $data->password);
    $query->bind_param('ssi', $data->username, $hashedPassword,  $data->status);
    $query->execute();
  } else if (!empty($data) && $data->type === 'UPDATE') {
    if ($data->password !== '') {
      $query = $conn->prepare('UPDATE users SET username = ?, password = ?, isAdmin = ?, updated_at = NOW() WHERE id = ?');
      $hashedPassword = hash('sha512', $data->password);
      $query->bind_param('ssdi', $data->username, $hashedPassword,  $data->status, $data->id);
    } else {
      $query = $conn->prepare('UPDATE users SET username = ?, isAdmin = ?, updated_at = NOW() WHERE id = ?');
      $query->bind_param('sid', $data->username, $data->status, $data->id);
    }
    $query->execute();
  } else if (!empty($data) && $data->type === 'DELETE') {

    $sql = "SELECT * FROM users WHERE id= ".intval($data->id);
    $result = mysqli_query($conn, $sql);
    $obj = mysqli_fetch_array($result);

    $query = $conn->prepare('DELETE FROM buyhistory WHERE username = ?');
    $query->bind_param('s', $obj["username"]);
    $query->execute();


    $query = $conn->prepare('DELETE FROM users WHERE id = ?');
    $query->bind_param('d', $data->id);
    $query->execute();
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=100%, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="">
  <title>Users Management</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="">
      <img src="images/bootstrap-solid.svg" width="30" height="30" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="nav navbar-nav">
        <!-- <li class="nav-item">
          <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link" href="products.php">Products</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="users.php">Users</a>
        </li>
        <li class="nav-item" style="right: 80px;position: absolute">
          <a class="nav-link" href="">User: <?php echo $_SESSION['username'] ?> </a>
        </li>
        <li class="nav-item" style="right: 10px;position: absolute">
          <a class="nav-link" href="login.php">log out</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="" id="app">
    <div class="container">
      <div class="row mt-3">
        <div class="col">
          <button class="btn btn-primary" data-toggle="modal" data-target="#createUserModal">Create New User</button>
          <button class="btn btn-success" v-on:click="reloadPage">Refresh table</button>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <table class="table table-hover mt-3">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Created at</th>
                <th scope="col">Updated at</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $users = $conn->query('SELECT * FROM users'); ?>
              <?php while ($row = $users->fetch_object()) {
                unset($row->password); ?>
                <tr>
                  <th scope="row"><?php echo $row->id ?></th>
                  <td><?php echo $row->username ?></td>
                  <td><?php echo date('Y-m-d H:i:s', strtotime($row->created_at)) ?></td>
                  <td><?php echo date('Y-m-d H:i:s', strtotime($row->updated_at)) ?></td>
                  <td><?php

                        $sql = "SELECT * FROM users WHERE username='" . $row->username   . "' ";
                        $result = mysqli_query($conn, $sql);
                        $obj = mysqli_fetch_array($result);
                        if ($obj['isAdmin'] == 1) {
                          echo "Admin";
                        } else {
                          echo "Member";
                        }


                        ?></td>
                  <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#updateUserModal" v-on:click='clickEditUser(<?php echo json_encode($row, JSON_FORCE_OBJECT); ?>)'>Edit</button>
                    <button type="button" class="btn btn-danger" v-on:click="deleteUser(<?php echo $row->id; ?>)">Delete</button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="modal" id="createUserModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Create a user</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" novalidate>
              <div class="form-group">
                <label for="username" class="col-form-label">Username:</label>
                <input type="text" class="form-control" placeholder="Enter Username"  v-model="username" required>
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">Password:</label>
                <input type="password" class="form-control" placeholder="Enter Password"  v-model="password" required>
              </div>
              <div class="form-group">
                <input v-model="status" type="radio" value=1 required>
                <label for="admin">Admin</label>
                <input v-model="status" type="radio" value=0 required>
                <label for="member">Member</label>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" v-on:click="createUser">Create</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="updateUserModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Update an user</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" novalidate>
              <div class="form-group">
                <label for="username" class="col-form-label">Username:</label>
                <input type="text" class="form-control" v-model="username" required>
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">New Password:</label>
                <input type="password" class="form-control" v-model="password" required>
              </div>
              <div class="form-group">
                <input v-model="status" type="radio" value=1 required>
                <label for="admin">Admin</label>
                <input v-model="status" type="radio" value=0 required>
                <label for="member">Member</label>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" v-on:click="updateUser">Update</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendors/bootstrap/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>

  <!-- Vue -->
  <script src="vendors/vue/vue.min.js"></script>

  <script>
    new Vue({
      el: "#app",
      data: function() {
        return {
          currentUrl: window.location.href,
          userId: null, // for reference on edit user
          username: "",
          password: "",
          status: 1
        };
      },
      methods: {
        createUser: function() {
          let payload = {
            username: this.username,
            password: this.password,
            status: this.status,
            type: "CREATE"
          };
          fetch(this.currentUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(payload)
            })
            .then(response => this.reloadPage())
            .catch(error => {
              console.error("Error:", error);
              alert(error.message);
            });
        },
        updateUser: function() {
          let payload = {
            id: this.userId,
            username: this.username,
            password: this.password,
            status: this.status,
            type: "UPDATE"
          };
          fetch(this.currentUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(payload)
            })
            .then(response => this.reloadPage())
            .catch(error => {
              console.error("Error:", error);
              alert(error.message);
            });
        },
        deleteUser: function(id) {
          const confirmForDelete = confirm(
            `Are you want to delete user id ${id}?`
          );
          if (confirmForDelete) {
            let payload = {
              id: id,
              type: "DELETE"
            };
            fetch(this.currentUrl, {
                method: "POST",
                body: JSON.stringify(payload)
              })
              .then(response => this.reloadPage())
              .catch(error => {
                console.error("Error:", error);
                alert(error.message);
              });
          }
        },
        clickEditUser: function(id) {
          this.userId = user.id;
          this.username = user.username;
        },
        clickEditUser: function(user) {
          this.userId = user.id;
          this.username = user.username;
        },
        reloadPage: function() {
          window.location.reload();
        },
      }
    });
  </script>
</body>

</html>