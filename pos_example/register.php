<?php
define('TIMEZONE', 'Asia/Bangkok');
$conn = new mysqli('localhost', 'root', '', 'pos_example');

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$usernameErr = "";
$passwordErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_POST["password"] !== $_POST["checkPassword"]) {
        $passwordErr = "Not match password";
    } else if ($_POST["username"] === "") {
        $usernameErr = "Username is required";
    } else {
        $query = $conn->prepare('INSERT INTO users (username, password, created_at, updated_at, login_status,last_activity_time, isAdmin) VALUES (?, ?, NOW(), NOW(), 0, "0000-00-00 00:00:00", 0)');
        $hashedPassword = hash('sha512', $_POST["password"]);
        $query->bind_param('ss', $_POST["username"], $hashedPassword);
        $query->execute();
        header("Location: login.php");
    }

}
$conn->close();

?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="vendors/css/registerPage.css">
    <title>Register Game Store Page</title>
</head>

<body>
    <div class="web-bg"></div>
    <div class="container">
        <div class="register-box">
            <div class="row">
                <p style="font-size: 65px; color: #12335a; margin-top: -50%">OS2</p>
                <h6 style="text-align: center">Please register by completing the information below</h6>
                <br>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" id="registerForm">
                    <div class="form-group">
                        <label for="inputUserName">Username</label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Enter username" >
                        <p style="color: red"> <?php echo $usernameErr ?></p>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                    </div>
                    <div class="form-group">
                        <label for="comfirmPassword">Confirm Password</label>
                        <input type="password" name="checkPassword" class="form-control" id="checkPassword" placeholder="Confirm password">
                        <p style="color: red"> <?php echo $passwordErr ?></p>
                    </div>
                    <button type="summit" class="btn" form="registerForm">Register</button>
                </form>
                <br>
                <hr style="border: 1.5px solid #3a3a3bbb">
                <a class="login-link" href="login.php">Have an Account?</a>
                <span>Please sign up</span>
            </div>
        </div>
    </div>

</body>

</html>