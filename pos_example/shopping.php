<?php
session_start();
if (!isset($_SESSION['username']) || $_SESSION['isAdmin'] == 1) {
  header("Location: login.php");
} else {
  define('TIMEZONE', 'Asia/Bangkok');
  $conn = new mysqli('localhost', 'root', '', 'pos_example');
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  if (!empty($data) && count($data->products) >= 1 && $data->type === 'CHECKOUT') {
  
    while ($product = array_shift($data->products)) {
      $query =  $conn->prepare('INSERT INTO buyhistory (username ,productName, price, amount, create_at) VALUES (? ,? ,?, ?, NOW())');
      $query->bind_param('ssdi', $_SESSION['username'], $product->name, $product->price, $product->amount);
      $query->execute();

      $query = $conn->prepare('UPDATE products SET amount = amount - ? WHERE id = ?');
      $query->bind_param('dd', $product->amount, $product->id);
      $query->execute();
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=100%, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="">
  <title>Shopping Page</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="shopping.php">
      <img src="./images/pokemon-ball-icon-4.jpg" width="30" height="30" alt="">
      Game Store
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="shopping.php">Shopping</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="buyhistory.php">Buy History</a>
        </li>
        <li class="nav-item" style="right: 80px;position: absolute">
          <a class="nav-link" href="">User: <?php echo $_SESSION['username'] ?> </a>
        </li>
        <li class="nav-item" style="right: 10px;position: absolute">
          <a class="nav-link" href="login.php">log out</a>
        </li>
      </ul>
    </div>
  </nav>

  <div id="app">
    <div class="container">
      <div class="row mt-3">
        <div class="col">
          <h1>Shopping Cart</h1>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product Description</th>
                <th scope="col">Product Price</th>
                <th scope="col">Product Amount</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="product in shoppingCart">
                <th scope="row">{{ product.id }}</th>
                <td>{{ product.name }}</td>
                <td>{{ product.description }}</td>
                <td>{{ product.price }}</td>
                <td>{{ product.amount }}</td>
                <td>
                  <button type="button" class="btn btn-danger" v-on:click="removeProductFromCart(product.id)">Remove from cart</button>
                </td>
              </tr>
              <tr v-if="shoppingCart.length > 0">
                <td></td>
                <td></td>
                <td></td>
                <td>Total price: {{ shoppingCartTotalPrice }}</td>
                <td>Total amount: {{ shoppingCartTotalAmount }}</td>
                <td>
                  <button type="button" class="btn btn-success" v-on:click="checkout">Checkout</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col">
          <h1>Products</h1>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Product Image</th>
                  <th scope="col">Product Name</th>
                  <th scope="col">Product Description</th>
                  <th scope="col">Product Price</th>
                  <th scope="col">Product Amount</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php $products = $conn->query('SELECT * FROM products'); ?>
                <?php while ($row = $products->fetch_object()) {

                  $sql = "SELECT * FROM promotion WHERE productName='" . $row->name . "' ";
                  $result = mysqli_query($conn, $sql);
                  $isSale = 0;
                  $obj = "";
                  if (mysqli_num_rows($result)) {
                    $isSale = 1;
                    $obj = mysqli_fetch_array($result);
                  }

                  ?>
                  <tr>
                    <th scope="row"><?php echo $row->id ?></th>
                    <td> <?php
                            echo "<img src= './" . $row->path_to_images . "'  width='100' height='100' > ";

                            ?>
                    </td>
                    <td> <?php
                            if ($isSale) {
                              echo "<div class='row'><p style='color:red'> SALE  (" . $obj["discountPercent"] . " %) </p>&nbsp;&nbsp;" . $row->name . '</div>';
                            } else {
                              echo $row->name;
                            }
                            ?>
                    </td>
                    <td><?php echo $row->description ?></td>
                    <td><?php

                          if ($isSale) {

                            echo "<div class='row'>" .  $obj["original_price"] . "-> <p style='color:red'>" . $row->price . "</p>";
                          } else {
                            echo $row->price;
                          }

                          ?>
                    </td>
                    <td><?php echo $row->amount ?></td>

                    <td>
                      <button type="button" class="btn btn-primary" v-on:click='addProductToCart(<?php echo json_encode($row, JSON_FORCE_OBJECT); ?>)'>Add to cart</button>
                    </td>
                  </tr>
                <?php } ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="vendors/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendors/bootstrap/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>

  <!-- Vue -->
  <script src="vendors/vue/vue.min.js"></script>

  <!-- lodash -->
  <script src="vendors/lodash/lodash.min.js"></script>

  <script>
    new Vue({
      el: "#app",
      computed: {
        shoppingCartTotalPrice: function() {
          let totalPrice = 0;
          for (let i = 0; i < this.shoppingCart.length; i++) {
            totalPrice += parseFloat(this.shoppingCart[i].price) * parseFloat(this.shoppingCart[i].amount);
          }
          return totalPrice;
        },
        shoppingCartTotalAmount: function() {
          let totalAmount = 0;
          for (let i = 0; i < this.shoppingCart.length; i++) {
            totalAmount += parseInt(this.shoppingCart[i].amount);
          }
          return totalAmount;
        }
      },
      data() {
        return {
          currentUrl: window.location.href,
          shoppingCart: [],
        }
      },
      methods: {
        addProductToCart: function(product) {
          let productInCarts = this.shoppingCart.find(cartProduct => cartProduct.id === product.id);
          if (!productInCarts) {
            let clonedProduct = _.cloneDeep(product);

            clonedProduct.amount = 1;
            this.shoppingCart.push(clonedProduct);
          } else {
            productInCarts.amount++
          }
        },
        removeProductFromCart: function(id) {
          let product = this.shoppingCart.find(product => product.id === id)
          if (product.amount < 2) {
            this.shoppingCart = this.shoppingCart.filter(cartProduct => cartProduct !== product);
          } else {
            product.amount -= 1;
          }
        },
        checkout: function() {
          let payload = {
            products: this.shoppingCart,
            type: "CHECKOUT"
          };

          fetch(this.currentUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(payload)
            })
            .then(response => {
              console.log("Success:", JSON.stringify(response));
              alert('Checkout success.');
              this.reloadPage();
            })
            .catch(error => {
              console.error("Error:", error);
              alert(error.message);
            });
        },
        reloadPage: function() {
          window.location.reload();
        },
      },
    })
  </script>
</body>
</html>
