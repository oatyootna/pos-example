<?php
session_start();
if (!isset($_SESSION['username']) || $_SESSION['isAdmin'] == 1) {
  header("Location: login.php");
} else {
  define('TIMEZONE', 'Asia/Bangkok');
  $conn = new mysqli('localhost', 'root', '', 'pos_example');
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  if (!empty($data) && $data->type === 'DELETE') {
    $query = $conn->prepare('DELETE FROM buyhistory WHERE id = ?');
    $query->bind_param('d', $data->id);
    $query->execute();
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=100%, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="">
  <title>History Page</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="">
    <img src="./images/pokemon-ball-icon-4.jpg" width="30" height="30" alt="">
      Game Store
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="shopping.php">Shopping </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="buyhistory.php">Buy History</a>
        </li>
        <li class="nav-item" style="right: 80px;position: absolute">
          <a class="nav-link" href="">User: <?php echo $_SESSION['username'] ?> </a>
        </li>
        <li class="nav-item" style="right: 10px;position: absolute">
          <a class="nav-link" href="login.php">log out</a>
        </li>
      </ul>
    </div>
  </nav>

  <div id="app">
    <div class="container">
      <div class="row mt-3">
        <div class="col">
          <h1>Shopping History</h1>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Product Images</th>
                  <th scope="col">Product Name</th>
                  <th scope="col">Product Price</th>
                  <th scope="col">Product Amount</th>
                  <th scope="col">Bought Date</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php $products = $conn->query('SELECT * FROM buyhistory WHERE username = "'.$_SESSION['username'].'"'); ?>
                <?php while ($row = $products->fetch_object()) { ?>
                  <tr>
                    <th scope="row">
                      <?php  
                      
                      $re = $conn->query('SELECT path_to_images FROM products WHERE name = "'.$row->productName.'" limit 1');
                      $image = $re->fetch_object();
                      echo "<img src= './" . $image->path_to_images . "'  width='100' height='100' > ";?></th>
                    <td><?php echo $row->productName ?></td>
                    <td><?php echo $row->price ?></td>
                    <td><?php echo $row->amount ?></td>
                    <td><?php echo $row->create_at ?></td>
                    <td>
                      <button type="button" class="btn btn-danger" v-on:click="deleteProduct(<?php echo $row->id ?>)">Delete History</button>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="vendors/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendors/bootstrap/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>

  <!-- Vue -->
  <script src="vendors/vue/vue.min.js"></script>

  <!-- lodash -->
  <script src="vendors/lodash/lodash.min.js"></script>

  <script>
    new Vue({
      el: '#app',
      data() {
        return {
          currentUrl: window.location.href,
          productId: "",
          productName: "",
          productDescription: "",
          productPrice: "",
          productAmount: "",
          discount: "",
          expireDate: "",
          file: "",
          pathToImages: "",
          isAddPromotion: false
        }
      },
      methods: {
        reloadPage: function() {
          window.location.reload();
        },
        deleteProduct: function(id) {
          const confirmForDelete = confirm(
            `Are you want to delete this history ?`
          );
          if (confirmForDelete) {
            let payload = {
              id: id,
              type: 'DELETE'
            };
            fetch(window.location.href, {
                method: "POST", // the method should be DELETE but to deal with php is too hard
                body: JSON.stringify(payload)
              })
              .then(response => this.reloadPage())
              .catch(error => {
                console.error("Error:", error);
                alert(error.message);
              });
          }
        }
      }
    });
  </script>
</body>

</html>