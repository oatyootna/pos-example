<?php
session_start();

if (!isset($_SESSION['username'])  || $_SESSION['isAdmin'] == 0) {
  header("Location: login.php");
} else {
  define('TIMEZONE', 'Asia/Bangkok');
  $conn = new mysqli('localhost', 'root', '', 'pos_example');
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  if (!empty($data) && $data->type === 'CREATE') {

    $disPrice = $data->productPrice;

    if ($data->isAddPromotion) {
      $query =  $conn->prepare('INSERT INTO promotion (productName, discountPercent, original_price, created_at, expired_date) VALUES (?, ?, ?,  NOW(), ?)');
      $query->bind_param('sids', $data->productName, $data->discount,  $data->productPrice, $data->expireDate);
      $query->execute();
      $disPrice =    $data->productPrice - (($data->productPrice *  $data->discount) / 100);
    }
    $query = $conn->prepare('INSERT INTO products (name, description, price, amount, created_at, updated_at,  path_to_images) VALUES (?, ?, ?, ?, NOW(), NOW(), ?)');
    $query->bind_param('ssdds', $data->productName, $data->productDescription,  $disPrice, $data->productAmount, $data->pathToImages);
    $query->execute();
  } else if (!empty($data) && $data->type === 'UPDATE') {

    if ($data->isAddPromotion) {

      $sql = "SELECT * FROM promotion WHERE productName='" . $data->productName . "'";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result)) {
        $proObj = mysqli_fetch_array($result);

        if ($proObj['original_price'] != $data->productPrice) {
          $query = $conn->prepare("UPDATE promotion SET discountPercent = ?, original_price = ?, created_at = NOW(), expired_date = ? WHERE productName = '" . $data->productName . "' ");
          $query->bind_param('ids', $data->discount, $data->productPrice, $data->expireDate);
          $query->execute();
        } else {
          $query = $conn->prepare("UPDATE promotion SET discountPercent = ?, created_at = NOW(), expired_date = ? WHERE productName = '" . $data->productName . "' ");
          $query->bind_param('is', $data->discount, $data->expireDate);
          $query->execute();
        }

        $disPrice =    $data->productPrice - (($data->productPrice *  $data->discount) / 100);

        if ($data->pathToImages == "images/undefined") {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ? WHERE id = ?');
          $query->bind_param('ssddd', $data->productName, $data->productDescription, $disPrice, $data->productAmount, $data->productId);
          $query->execute();
        } else {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ?, path_to_images = ? WHERE id = ?');
          $query->bind_param('ssddsd', $data->productName, $data->productDescription, $disPrice, $data->productAmount,  $data->pathToImages, $data->productId);
          $query->execute();
        }
      } else {

        $query =  $conn->prepare('INSERT INTO promotion (productName, discountPercent, original_price, created_at, expired_date) VALUES (?, ?, ?,  NOW(), ?)');
        $query->bind_param('sids', $data->productName, $data->discount,  $data->productPrice, $data->expireDate);
        $query->execute();

        $disPrice =    $data->productPrice - (($data->productPrice *  $data->discount) / 100);

        if ($data->pathToImages == "images/undefined") {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ? WHERE id = ?');
          $query->bind_param('ssddd', $data->productName, $data->productDescription, $disPrice, $data->productAmount, $data->productId);
          $query->execute();
        } else {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ?, path_to_images = ? WHERE id = ?');
          $query->bind_param('ssddsd', $data->productName, $data->productDescription, $disPrice, $data->productAmount, $data->pathToImages, $data->productId);
          $query->execute();
        }
      }
    } else {

      $sql = "SELECT * FROM promotion WHERE productName='" . $data->productName . "'";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result)) {
        $proObj = mysqli_fetch_array($result);


        if ($data->pathToImages == "images/undefined") {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ? WHERE id = ?');
          $query->bind_param('ssddd', $data->productName, $data->productDescription, $proObj['original_price'], $data->productAmount, $data->productId);
          $query->execute();
        } else {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ?, path_to_images = ? WHERE id = ?');
          $query->bind_param('ssddsd', $data->productName, $data->productDescription, $proObj['original_price'], $data->productAmount, $data->pathToImages, $data->productId);
          $query->execute();
        }

        $query = $conn->prepare('DELETE FROM promotion WHERE productName = ?');
        $query->bind_param('d', $data->productName);
        $query->execute();
      } else {

        if ($data->pathToImages == "images/undefined") {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ? WHERE id = ?');
          $query->bind_param('ssddd', $data->productName, $data->productDescription, $data->productPrice, $data->productAmount, $data->productId);
          $query->execute();
        } else {
          $query = $conn->prepare('UPDATE products SET name = ?, description = ?, price = ?, amount = ?, path_to_images = ? WHERE id = ?');
          $query->bind_param('ssddsd', $data->productName, $data->productDescription,  $data->productPrice, $data->productAmount, $data->pathToImages, $data->productId);
          $query->execute();
        }
      }
    }
  } else if (!empty($data) && $data->type === 'DELETE') {

    $query = $conn->prepare('DELETE FROM products WHERE id = ?');
    $query->bind_param('d', $data->id);
    $query->execute();

    $query = $conn->prepare('DELETE FROM promotion WHERE productName = ?');
    $query->bind_param('d', $data->productName);
    $query->execute();
  }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=100%, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="">
  <title>Products Management</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="">
      <img src="images/bootstrap-solid.svg" width="30" height="30" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <!-- <li class="nav-item">
          <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
        </li> -->
        <li class="nav-item active">
          <a class="nav-link" href="products.php">Products</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="users.php">Users</a>
        </li>
        <li class="nav-item" style="right: 80px;position: absolute">
          <a class="nav-link" href="">User: <?php echo $_SESSION['username'] ?> </a>
        </li>
        <li class="nav-item" style="right: 10px;position: absolute">
          <a class="nav-link" href="login.php">log out</a>
        </li>
      </ul>
    </div>
  </nav>

  <div id="app">
    <div class="container">
      <div class="row mt-3">
        <div class="col">
          <button class="btn btn-primary" data-toggle="modal" data-target="#createProductModal" v-on:click="resetCreateProductForm">Create New Product</button>
          <button class="btn btn-success" v-on:click="reloadPage">Refresh table</button>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <table class="table table-hover mt-3">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Product Image</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Amount</th>
                <th scope="col">Created at</th>
                <th scope="col">Updated at</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $products = $conn->query('SELECT * FROM products'); ?>
              <?php while ($row = $products->fetch_object()) {

                $sql = "SELECT * FROM promotion WHERE productName='" . $row->name . "' ";
                $result = mysqli_query($conn, $sql);
                $isSale = 0;
                $obj = "";
                if (mysqli_num_rows($result) > 0) {
                  $isSale = 1;
                  $obj = mysqli_fetch_array($result);
                }
                // $conn->close();
                ?>
                <tr>
                  <th scope="row"><?php echo $row->id ?></th>
                  <td> <?php
                          echo "<img src= './" . $row->path_to_images . "'  width='100' height='100' > ";

                          ?>
                  </td>
                  <td> <?php
                          if ($isSale) {
                            echo "<div class='row'><p style='color:red'> SALE  (" . $obj["discountPercent"] . " %) </p>&nbsp;&nbsp;" . $row->name . '</div>';
                          } else {
                            echo $row->name;
                          }
                          ?>
                  </td>
                  <td><?php echo $row->description ?></td>
                  <td><?php

                        if ($isSale) {

                          echo "<div class='row'>" .  $obj["original_price"] . "-> <p style='color:red'>" . $row->price . "</p>";
                        } else {
                          echo $row->price;
                        }

                        ?>
                  </td>
                  <td><?php echo $row->amount ?></td>
                  <td><?php echo date('Y-m-d H:i:s', strtotime($row->created_at)) ?></td>
                  <td><?php echo date('Y-m-d H:i:s', strtotime($row->updated_at)) ?></td>
                  <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#updateProductModal" v-on:click='clickEditProduct(<?php echo json_encode($row, JSON_FORCE_OBJECT) . "," . json_encode($obj, JSON_FORCE_OBJECT); ?>)'>Edit</button>
                    <button type="button" class="btn btn-danger" v-on:click="deleteProduct(<?php echo $row->id ?>)">Delete</button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="modal" id="createProductModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Create a product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>


                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="productName" class="col-form-label">Product Name:</label>
                      <input type="text" class="form-control" v-model="productName" required>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="" class="col-form-label">Upload Picture:</label>
                      <input type="file" ref="file1" v-on:change="handleFileUpload('c')" />
                    </div>
                  </div>
                </div>


                <div class="form-group">
                  <label for="productDescription" class="col-form-label">Product Description:</label>
                  <textarea class="form-control" v-model="productDescription" required></textarea>
                </div>
                <div class="form-group">
                  <label for="productPrice" class="col-form-label">Product Price:</label>
                  <input type="number" class="form-control" v-model="productPrice" required>
                </div>
                <div class="form-group">
                  <label for="productAmount" class="col-form-label">Product Amount:</label>
                  <input type="number" class="form-control" v-model="productAmount" min="0" required>
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-danger" v-on:click="clickPromotion" id="createPromotionBtn">Add Promotion</button>
                </div>
                <div class="form-group">
                  <div class="row" v-if="isAddPromotion">
                    <div class="col-sm-2"> <label for="discount" class="col-form-label">Discount: </label></div>
                    <div class="col-sm-3"><input type="number" class="form-control" style="width: 100px" v-model="discount" min="1"></div>
                    <div class="col-sm-1"> <label for="discount" class="col-form-label"> % </label></div>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <div class="row" v-if="isAddPromotion">
                    <div class="col-sm-3"> <label for="expireDate" class="col-form-label">Sale End At: </label></div>
                    <div class="col-sm-5"><input type="datetime-local" class="form-control" v-model="expireDate"></div>
                  </div>
                </div> -->
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" v-on:click="createProduct">Create</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" id="updateProductModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Update a product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="needs-validation" novalidate>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="productName" class="col-form-label">Product Name:</label>
                      <input type="text" class="form-control" v-model="productName" required>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="" class="col-form-label">Upload Picture:</label>
                      <input type="file" ref="file" v-on:change="handleFileUpload('u')" />
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="productDescription" class="col-form-label">Product Description:</label>
                  <textarea class="form-control" v-model="productDescription" required></textarea>
                </div>
                <div class="form-group">
                  <label for="productPrice" class="col-form-label">Product Price:</label>
                  <input type="number" class="form-control" v-model="productPrice" required>
                </div>
                <div class="form-group">
                  <label for="productAmount" class="col-form-label">Product Amount:</label>
                  <input type="number" class="form-control" v-model="productAmount" min="0" required>
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-danger" v-on:click="clickPromotion" id="updatePromotionBtn">Add Promotion</button>
                </div>
                <div class="form-group">
                  <div class="row" v-if="isAddPromotion">
                    <div class="col-sm-2"> <label for="discount" class="col-form-label">Discount: </label></div>
                    <div class="col-sm-3"><input type="number" class="form-control" style="width: 100px" v-model="discount" min="1"></div>
                    <div class="col-sm-1"> <label for="discount" class="col-form-label"> % </label></div>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <div class="row" v-if="isAddPromotion">
                    <div class="col-sm-3"> <label for="expireDate" class="col-form-label">Sale End At: </label></div>
                    <div class="col-sm-5"><input type="datetime-local" class="form-control" v-model="expireDate"></div>
                  </div>
                </div> -->

              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" v-on:click="updateProduct">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="vendors/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendors/bootstrap/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


  <!-- Vue -->
  <script src="vendors/vue/vue.min.js"></script>

  <script>
    new Vue({
      el: '#app',
      data() {
        return {
          currentUrl: window.location.href,
          productId: "",
          productName: "",
          productDescription: "",
          productPrice: "",
          productAmount: "",
          discount: "",
          expireDate: "",
          file: "",
          pathToImages: "",
          isAddPromotion: false
        }
      },
      methods: {
        createProduct: function() {
    
          let payload = {
            productName: this.productName,
            productDescription: this.productDescription,
            productPrice: this.productPrice,
            productAmount: this.productAmount,
            isAddPromotion: this.isAddPromotion,
            discount: this.discount,
            expireDate: this.expireDate,
            pathToImages: 'images/' + this.file['name'],
            type: 'CREATE'
          };
          fetch(this.currentUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(payload)
            })
            .then(response => this.reloadPage())
            .catch(error => {
              console.error("Error:", error);
              alert(error.message);
            });

        },
        reloadPage: function() {
          window.location.reload();
        },
        updateProduct: function() {

          let payload = {
            productId: this.productId,
            productName: this.productName,
            productDescription: this.productDescription,
            productPrice: this.productPrice,
            productAmount: this.productAmount,
            discount: this.discount,
            expireDate: this.expireDate,
            isAddPromotion: this.isAddPromotion,
            pathToImages: 'images/' + this.file['name'],
            type: 'UPDATE'
          };
          fetch(this.currentUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(payload)
            })
            .then(response => this.reloadPage())
            .catch(error => {
              console.error("Error:", error);
              alert(error.message);
            });

        },
        deleteProduct: function(id) {
          const confirmForDelete = confirm(
            `Are you want to delete product id ${id}?`
          );
          if (confirmForDelete) {
            let payload = {
              id: id,
              type: 'DELETE'
            };
            fetch(window.location.href, {
                method: "POST", // the method should be DELETE but to deal with php is too hard
                body: JSON.stringify(payload)
              })
              .then(response => this.reloadPage())
              .catch(error => {
                console.error("Error:", error);
                alert(error.message);
              });
          }
        },
        clickPromotion: function() {
          if (this.isAddPromotion) {
            this.isAddPromotion = false;
            this.discount = 1;
            $("#createPromotionBtn").html("Add Promotion");
            $("#updatePromotionBtn").html("Add Promotion");
          } else {
            this.isAddPromotion = true;
            $("#createPromotionBtn").html("Clear Promotion");
            $("#updatePromotionBtn").html("Clear Promotion");
          }
        },
        clickEditProduct: function(product, obj) {
          this.productId = product.id;
          this.productName = product.name;
          this.productDescription = product.description;
          this.productAmount = product.amount;
          this.productPrice = product.price;
          this.discount = 1;

          if (obj.discountPercent != null) {
            $("#updatePromotionBtn").html("Clear Promotion");
            this.isAddPromotion = true;
            this.productPrice = obj.original_price;
            this.discount = obj.discountPercent;
          } else {
            this.isAddPromotion = false;
            $("#updatePromotionBtn").html("Add Promotion");
          }

        },
        handleFileUpload(event) {
     
          if(event == 'c'){
            this.file = this.$refs.file1.files[0];
          }else{
            this.file = this.$refs.file.files[0];
          }

          let formData = new FormData();
          formData.append('file', this.file);

          axios.post('uploads.php', formData, {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            })
            .then(function(response) {

              if (!response.data) {
                console.log('File not uploaded.');
              } else {
                console.log('File uploaded successfully.');
              }

            })
            .catch(function(error) {
              console.log(error);
            });


        },
        resetCreateProductForm: function() {
          this.productId = '';
          this.productName = '';
          this.productDescription = '';
          this.productPrice = '';
          this.productAmount = '';
        }
      },
    });
  </script>
</body>

</html>