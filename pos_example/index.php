<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: login.php");
} 
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=100%, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="">
  <title>Point of Sale</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
</head>

<body>
  <div id="app">
    <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
      <a class="navbar-brand" href="index.php">
        <img src="images/bootstrap-solid.svg" width="30" height="30" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="nav navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="products.php">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Users</a>
          </li>
          <li class="nav-item" style="right: 80px;position: absolute">
            <a class="nav-link" href="">User: <?php echo $_SESSION['username'] ?> </a>
          </li>
          <li class="nav-item" style="right: 10px;position: absolute">
            <a class="nav-link" href="login.php">log out</a>
          </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item">
            <?php if (isset($_SESSION['username'])) { ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Welcome, <?php echo $_SESSION['username'] ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="logout.php">Logout</a>
            </div>
          </li>
        <?php } else { ?>
          <a class="nav-link" href="login.php">Login</a>
        <?php } ?>
        </li>
        </ul>
      </div>
    </nav>

    <div class="container">
    </div>
  </div>

  <script src="vendors/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendors/bootstrap/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>

  <!-- Vue -->
  <script src="vendors/vue/vue.min.js"></script>

  <script>
    new Vue({
      el: "#app"
    })
  </script>
</body>

</html>