# POS Example

POS(Point of Sale), a simple CRUD application written in native php and [Vue.js](https://vuejs.org).

## Features

- CRUD for products
- CRUD for users
- CRUD for shopping

## Schema

![schema](docs/images/schema.png)

## Folder Strcutures

| Folder      | Detail              |
|-------------|---------------------|
| pos_example | source code for pos |
| docs        | documents           |
| database    | database file       |
